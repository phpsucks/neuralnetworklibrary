import numpy as np


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def sigmoid_der(x):
    return x * (1 - x)


def s_sin(x):
    return np.square(np.sin(x))


def s_sin_der(x):
    return np.sin(2 * x)


def softmax(x):
    a = np.exp(x - np.max(x))
    return a / a.sum(axis=0)


def relu(x):
    return np.maximum(0, x)


def relu_der(x):
    x[x <= 0] = 0
    x[x > 0] = 1
    return x


def single_feed(w, b, inp, act):
    out = np.dot(inp, w) + b
    return act(out)


class neuralNet():
    def __init__(self, architecture, activation):
        self.layers = len(architecture)
        self.architecture = architecture
        self.activations = activation
        self.act = sigmoid
        self.opt = sigmoid_der
        self.features = {}
        self.memory = {}
        self.delta_weights = {}
        self.start()

    def start(self):
        p = 0
        for i in range(self.layers - 1):
            p = i + 1
            self.features['W' + str(i)] = np.random.randn(self.architecture[i], self.architecture[i + 1])
            self.features['B' + str(i)] = np.ones((1, self.architecture[p]))

    def checkFnc(self, i):
        if self.activations[i] == 'sigmoid':
            self.act = sigmoid
        elif self.activations[i] == 's_sin':
            self.act = s_sin
        elif self.activations[i] == 'softmax':
            self.act = softmax
        elif self.activations[i] == 'relu':
            self.act = relu
        else:
            raise Exception("Function '" + self.activations[i] + "' not supported (yet)")

    def checkOpt(self, i):
        if self.activations[i] == 'sigmoid':
            self.opt = sigmoid_der
        elif self.activations[i] == 's_sin':
            self.opt = s_sin_der
        elif self.activations[i] == 'softmax':
            self.opt = sigmoid_der
        elif self.activations[i] == 'relu':
            self.opt = relu_der
        else:
            raise Exception("Optimizer '" + self.activations[i] + "' not supported (yet)")

    def feed(self, inp):
        self.memory['m0'] = inp  # adding inputs to memory

        for i in range(self.layers - 1):
            self.checkFnc(i)  # check for desired activation

            outp = single_feed(self.features['W' + str(i)], self.features['B' + str(i)], inp, self.act)
            self.memory['m' + str(i + 1)] = outp
            inp = outp
            # print(inp)

        self.estimation = inp

    def single_back(self, e, i):
        self.opt(i)
        de = e * self.opt(self.memory['m' + str(i)])

        self.features['W' + str(i - 1)] += self.memory['m' + str(i - 1)].T.dot(de)

        # self.features['B' + str(i - 1)] = (a + self.features['B' + str(i - 1)].T).T
        # TODO bias update

    def back_propagation(self, output):

        self.checkOpt(self.layers - 2)

        error = output - self.estimation

        de = error * self.opt(self.estimation)
        self.features['W' + str(self.layers - 2)] += self.memory['m' + str(self.layers - 2)].T.dot(de)

        for i in range(self.layers - 2, 0, -1):
            # print(i)

            try:
                e = np.dot(de, self.features['W' + str(i)].T)
            except:
                e = np.dot(de, self.features['W' + str(i)])
            self.single_back(e, i)
            de = e
        # print('Done!')

    def run(self, x):
        self.feed(x)
        return self.estimation

    def train(self, inp, out, epochs):
        for i in range(epochs):
            self.feed(inp)
            self.back_propagation(out)

    def model_save(self):
        for i in range(self.layers - 1):
            np.save('model/W' + str(i), self.features['W' + str(i)])

    def model_load(self):
        for i in range(self.layers - 1):
            self.features['W' + str(i)] = np.load('model/W' + str(i) + '.npy')


