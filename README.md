# NeuralNetworkLibrary

This is a simple library beginners can use to get the basic understanding of how neural nets work.
It has support for any number of layers (as far as I've tested), you can choose your activation and optimization functions.
More features are getting added soon!

**USE:**
Here is a simple guide to use the library

1. Import the library (it has to be in the same directory)

    - `from ANN import neuralNet`
2. init an instance of the net(in this case i will name it 'brain') first parameter is and touple containing layers and amount of nodes per layers, second is the activation functions in a touple
    - `brain = neuralNet((2,2,1), ('sigmoid', 'sigmoid'))`
3. make get inputs and results for training like:
     
```
inputs = np.array([[0, 0],
                   [0, 1],
                   [1, 0],
                   [1, 1]])

        outputs = np.array([[1],
                    [0],
                    [0],
                    [1]])
```
4. train the network. It take 3 params -> inputs, outputs and epochs(how many times to repeat the training)
    - `brain.train(inputs, outputs, 1000)`
5. To test the model you need your testing data:
`x = np.array([0, 0])`
6. finally add `brain.run(x)` and `print(brain.estimation)` to display the result of the test data

7. additionally you can save the model by adding `brain.model_save()` and load it next time by adding `brain.model_load()`
 

I hope this helps someone out there... 


Written in python by Luka Knapic
