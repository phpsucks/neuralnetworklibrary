
import numpy as np
from neuralnetworklibrary.ANN import neuralNet

EPOCHS = 10000

#sigmoid is currently the most stable(softmax as well but its only used for end computation)
#s_sin not stable
brain = neuralNet((2, 5, 4, 1), ('sigmoid', 'sigmoid', 'sigmoid'))
# OPTIONS: s_sin, sigmoid, relu, softmax. num_of_layers-1

# TODO bug with relu activation
# TODO implement bias update
# TODO make derivatives of chosen functions update automaticly
# TODO add option for custom optimizer


inputs = np.array([[0, 0],
                   [0, 1],
                   [1, 0],
                   [1, 1]])

outputs = np.array([[1],
                    [0],
                    [0],
                    [1]])

brain.train(inputs, outputs, EPOCHS)
#brain.model_save()

#brain.model_load()

x = np.array([0, 0])

brain.run(x)
print(brain.estimation)
